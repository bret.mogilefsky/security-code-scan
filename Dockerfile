FROM microsoft/dotnet:2.0-sdk
COPY analyzer /
ENTRYPOINT []
CMD ["/analyzer", "run"]
