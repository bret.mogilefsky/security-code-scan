package main

import (
	"bytes"
	"errors"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	pathDotnet          = "/usr/bin/dotnet"
	pkgSecurityCodeScan = "SecurityCodeScan"
)

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Env = os.Environ()
		cmd.Stderr = os.Stderr
		cmd.Dir = path
		return cmd
	}

	// Get basename of project file
	proj, err := findProjectFile(path)
	if err != nil {
		return nil, err
	}

	// Clean project
	if err := setupCmd(exec.Command(pathDotnet, "clean")).Run(); err != nil {
		return nil, err
	}

	// Add SecurityCodeScan package
	if err := setupCmd(exec.Command(pathDotnet, "add", proj, "package", pkgSecurityCodeScan)).Run(); err != nil {
		return nil, err
	}

	// Build project and save output
	cmd := exec.Command(pathDotnet, "build")
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr
	cmd.Dir = path
	stdout, err := cmd.Output()
	if err != nil {
		// Ignore exit error
		if _, isExitErr := err.(*exec.ExitError); !isExitErr {
			return nil, err
		}
	}
	buf := bytes.NewBuffer(stdout)
	return fakeCloser{buf}, nil
}

type fakeCloser struct {
	io.Reader
}

func (f fakeCloser) Close() error {
	return nil
}

// findProjectFile returns the name of the project file found in given directory.
func findProjectFile(dir string) (string, error) {
	f, err := os.Open(dir)
	if err != nil {
		return "", err
	}
	defer f.Close()

	names, err := f.Readdirnames(-1)
	if err != nil {
		return "", err
	}
	for _, file := range names {
		switch filepath.Ext(file) {
		case ".csproj", ".vbproj":
			return file, nil
		default:
			// ignore
		}
	}
	return "", errors.New("Project file not found in directory:" + dir)
}
